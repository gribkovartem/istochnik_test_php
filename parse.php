<?php

require __DIR__ . '/vendor/autoload.php';

/**
 * Подготавливаем данные
 */
$email = 'gribkovartem@gmail.com';
$url = 'http://hires.istochnik.im/candles?email=' . $email;
$json_data = file_get_contents($url);
$parsed_data = json_decode($json_data);
$columns = $parsed_data->data->candles->columns; //колонки
$data = $parsed_data->data->candles->data; //данные

/**
 * Подготавливаем таблицу
 */
$php_excel = new PHPExcel();
$page = $php_excel->setActiveSheetIndex(0);
$cell_style = [
    'font' => [
        'bold' => true
    ]
];
$page->setCellValue('B1', 'open');
$page->getStyle('B1')->applyFromArray($cell_style);
$page->setCellValue('C1', 'close');
$page->getStyle('C1')->applyFromArray($cell_style);
$page->setCellValue('D1', 'high');
$page->getStyle('D1')->applyFromArray($cell_style);
$page->setCellValue('E1', 'low');
$page->getStyle('E1')->applyFromArray($cell_style);
$page->setCellValue('F1', 'value');
$page->getStyle('F1')->applyFromArray($cell_style);

/**
 * Заполняем таблицу данными
 */
$row = 2;
foreach ($data as $data_item) {
    $begin = $data_item[array_search('begin', $columns)];
    $open = $data_item[array_search('open', $columns)];
    $close = $data_item[array_search('close', $columns)];
    $high = $data_item[array_search('high', $columns)];
    $low = $data_item[array_search('low', $columns)];
    $value = $data_item[array_search('value', $columns)];

    $page->setCellValueByColumnAndRow(0, $row, $begin);
    $page->setCellValueByColumnAndRow(1, $row, $open);
    $page->setCellValueByColumnAndRow(2, $row, $close);
    $page->setCellValueByColumnAndRow(3, $row, $high);
    $page->setCellValueByColumnAndRow(4, $row, $low);
    $page->setCellValueByColumnAndRow(5, $row, $value);
    ++$row;
}

/**
 * Сохраняем таблицу
 */
$page->setTitle('Источник тестовое задание 1');
$excel_file = PHPExcel_IOFactory::createWriter($php_excel, 'Excel2007');
$excel_file->save(date('Y-m-d_H-i-s') . '.xlsx');

